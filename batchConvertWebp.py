#!/usr/bin/env python3
"""\nThis file can search multiple directory's recursively for files that are
  .webp and converts them and removes the original .webp file.

   This script requires PIL aka python image library (Pillow) to convert images
       Install using pip like so:
           pip install Pillow

 Usage:

    $ python batchConvertWebp.py /your/directory /your/directory2 .jpg

  This file accepts two different kinds of arguments, both of which are
  optional.

  1: Directory or directories to search recursively. Will search every
     subdirectory in the directory's you pass. The default is the users entire
     $HOME directory.

  2: File format that the .webp is to be converted to.

     You can pass .bmp .jpg, .jpeg or .png. Defaults to .png to keep
     transparency.
       Note: Transparency needs testing. I have not found transparent .webp yet

         $ python batchConvertWebp.py /your/directory .bmp

     If you prefer you can use the keyword argument -> format

         Windows:

            $ python batchConvertWebp.py C:\\\\your\\directory format:.jpeg

         *nix:

            $ python batchConvertWebp.py /your/directory format=.jpg

               Note: *nix can use windows format but I don't think Windows can
                     use the '=' sign in cmd-prompt? Don't have windows to test
"""

from sys import argv, exit as sExit
from os import walk, remove
from os.path import splitext, expanduser, isdir, join as oJoin
from PIL import Image

def convert_webps(searchDir=expanduser('~'), imgFormat='.png'):
    """ This function searches the entire directory for files that are .webp
and converts them to format and removes the original .webp file. File base-name
will remain unchanged. """
    for path, _, files in walk(searchDir):
        for file_ in files:
            if splitext(file_)[1] == '.webp':
                ## Try: see if we have permission to write to file system.
                try:
                    print(oJoin(path, file_))
                    im = Image.open(oJoin(path, file_)).convert('RGB')
                    if imgFormat == '.jpg':
                        im.save(oJoin(path,
                                splitext(file_)[0] + ".jpg"), 'jpeg')
                    else:
                        im.save(oJoin(path,
                                splitext(file_)[0] + imgFormat), imgFormat[1:])
                    im.close()
                    remove(oJoin(path, file_))
                except PermissionError as p:
                    raise p


if __name__ == '__main__':

    if True in (True for x in ('-h', '--help') if x in argv[1:]):
        ## Let's do "Get Help"!
        sExit(__doc__)

    ## Image types we want to convert to.
    imgTypes = ['.png', '.jpg', '.jpeg', '.bmp']

    ## Clean out the keyword argument variables and separators.
    args = [x for x in argv[1:] if '=' not in x and ':' not in x]

    ## Create the key value pairs from ':', or '=' separators.
    karg = {x.split(y, 1)[0]: x.split(y, 1)[1] for x in argv[1:]
            for y in ['=', ':'] if y in x}

    ## Search for image format type.
    format_ = [x for x in args for y in imgTypes if y in x]

    ## Test if the format list is empty and return none. if not use it's first
    ## value.
    format_ = format_[0] if format_ != [] else None

    ## Assign the format in the keyword arguments and use it if present.
    ## overrides the arg from args if both are passed
    format_ = (karg['format'] if 'format' in karg.keys() and 
               karg['format'] in imgTypes else format_)

    ## Make sure that the format_ has a value.
    format_ = format_ if format_ is not None else '.png'

    ## Clean out the imgTypes from the args if there.
    args = [x for x in args if x not in imgTypes]

    # print(f" arg: {args}\n karg: {karg}\n format: {format_}")

    if args != []:
        for arg in args:
            if isdir(arg):
                convert_webps(arg, imgFormat=format_)
    else:
        convert_webps(imgFormat=format_)
