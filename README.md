# batch convert webp


## Getting started

This file can search multiple directory's recursively for files that are
  .webp and converts them and removes the original .webp file.

   This script requires PIL aka python image library (Pillow) to convert images
       Install using pip like so:
           pip install Pillow  

## Usage:

    $ python batchConvertWebp.py /your/directory /your/directory2 .jpg

  This file accepts two different kinds of arguments, both of which are 
  optional.

  1: Directory or directories to search recursively. Will search every
     subdirectory in the directory's you pass. The default is the users entire
     $HOME directory.

  2: File format that the .webp is to be converted to.

     You can pass .bmp .jpg, .jpeg or .png. Defaults to .png to keep 
     transparency.
       Note: Transparency needs testing. I have not found transparent .webp yet!

         $ python batchConvertWebp.py /your/directory .bmp

## If you prefer you can use the keyword argument -> format

### Windows:

        $ python batchConvertWebp.py C:\\\\your\\directory format:.jpeg

### *nix:

        $ python batchConvertWebp.py /your/directory format=.jpg

           Note: *nix can use windows format but I don't think Windows can 
                 use the '=' sign in cmd-prompt? Don't have windows to test.